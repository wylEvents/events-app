//
//  AppDelegate.h
//  Events-App
//
//  Created by Wesley Araujo on 7/24/15.
//  Copyright (c) 2015 WYL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

