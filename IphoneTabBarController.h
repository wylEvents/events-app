//
//  IphoneTabBarController.h
//  Events-App
//
//  Created by Yeltsin Suares Lobato Gama on 7/28/15.
//  Copyright (c) 2015 WYL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IphoneTabBarController : UITabBarController

@end
