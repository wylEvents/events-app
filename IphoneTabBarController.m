//
//  IphoneTabBarController.m
//  Events-App
//
//  Created by Yeltsin Suares Lobato Gama on 7/28/15.
//  Copyright (c) 2015 WYL. All rights reserved.
//

#import "IphoneTabBarController.h"

@interface IphoneTabBarController ()

@end

@implementation IphoneTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
